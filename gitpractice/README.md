git commands
    If unsure, type git into bash

git init
    To link a project (working directory) to git, first cd into that project folder then run git init.
    This installs a hidden file called .git which tracks all files in that folder (ls -a reveals it)

git add . or git add <fileName> or git add *.<fileType> - this sends the files to the staging area
    git add . is to make git track all files in that directory
    git add <fileName> - to make git track only that file
    git add *.html adds all html files in directory
    git add -A adds everything in the directory including hidden files and folders 

git rm --cached <fileName> or git reser HEAD <fileName>
    To remove an added file

git ignore
    These files are never added, even with git add .
    create a .gitignore file in your directory. In it add the names of the files to ignore
    You can also include *.<fileType> to ignore all files of a type
    
git status
    see what is ready to be commited (staged)

git commit -m "message"
    way to commit added files to be tracked by git. 
    The message should be present tense and descriptive of the actual modifications made.

git log
    Shows the hash keys, data, and message of all commits
    
git branch
    Show all the branches in the directory. 
    
git checkout -b <branchName>
    Create a new branch. Fork your work into another instance. Develop it in parallel to the main branch. 
    
git checkout <branchName>
    Enter into any other branch you made.

git merge <branchName>
    Merges branch2 into branch1. Check into branch1 to do this. 
    
-- Login to gitHub and make a new repository
git remote add "repository URL from gitHub"
    
git remote 
    Shows what remote repositories (ex: ones from gitHub) are connected
    
git push -u origin master
    Push commits to remote repository. Thereafter, any future changes are added to gitHub with git push
    After the usual git add . and git commit -m "description"
    
git clone <clone URL from gitHub>
    To open your work from gitHub, click Clone or download, copy the URL, then Clone the repository into your workspace
    
git pull 
    When a team is working on a project, this brings the project up to date on your workspace
    (pulls latest commits from remote repository)
    
git revert --no-commit <hash#>..HEAD
    reverts to a hash specified commit. The hash$ you get from typing gitlog# My project's README
